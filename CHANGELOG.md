# SINABS change log

## v 0.2

- TorchLayer renamed to Layer
    - Added a depricated class TorchLayer with warning
- Added some new layer types
    - BPTT enabled spike activation layer
    - SpikingTemporalConv1dLayer