sinabs.SynOpCounter
===================

.. automodule:: sinabs.synopcounter
.. py:currentmodule:: sinabs.synopcounter


`SNNSynOpCounter`
~~~~~~~~~~~~~~~~~

.. autoclass:: SNNSynOpCounter
    :members:


`SynOpCounter`
~~~~~~~~~~~~~~

.. autoclass:: SynOpCounter
    :members:
